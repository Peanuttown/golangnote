package main

import "fmt"
import "time"

func main() {
	var t = make(chan int, 1)
	go func() {
		time.Sleep(time.Second * 3)
		close(t)
	}()
	select {
	case <-t:
		fmt.Println("12")
	}
}
