package tzzbufio

import (
	"io"
)

/*
bufio.Reader three main elememts
1: buf []byte   --- is to save the buf data

2: rd io.Reader ----  is the underly reader which we read data from it and save the data to the buf

3: r,w int   ------   they are two point to indicate the buf data we read at and write at.it means:
		if we read data from underly reader to our buf ,the w point will increase the num which we read from underly reader.
		if we read data from our buf to the destination data ,thw r point will increase
*/

//we define the smallest bufSize

type TzzReader struct {
	buf  []byte    //our buf data to save data which we read from underly reader
	rd   io.Reader //the underly reader
	r, w int
}

func NewTzzReader(bufSize int, rd io.Reader) *TzzReader {
	r := new(TzzReader)
	r.buf = make([]byte, bufSize)
	r.rd = rd
	return r
}

func (this *TzzReader) Read(p []byte) (n int, err error) {
	n = len(p)
	if this.r == this.w { //the buf data has been all read or this.buf is empty.so we read data from the underly reader
		if n >= len(this.buf) {
			//out buf is empty or has been all read ,it means ,the next data we need is in the underly reader
			//and the len(p) is larger than our bufer ,so we do not need to firstly copy the data to out buf then copy to p
			//we just copy the data from the underly reader to p
			n, err = this.rd.Read(p)
			return
		}
		//read data from the uderly reader to out buf
		n, err = this.rd.Read(this.buf)
		if err != nil { //read wrong
			return
		}
		//read to p
		this.r, this.w = 0, 0
		this.w += n
	}
	n = copy(p, this.buf[this.r:this.w])
	this.r += n
	return
}

func (this *TzzReader) fill() error {
	copy(this.buf, this.buf[this.r:this.w]) //sliding the  exsiting and unread data to beginning
	this.w -= this.r
	this.r = 0

	n, err := this.rd.Read(this.buf[this.w:])
	if err != nil {
		return err
	}
	this.w += n
	return nil
}

func (this *TzzReader) Peek(n int) ([]byte, error) {
	// if n is larger than the data int out buf
	//fill our buf, if this.w-this.r = len(this.buf) ,this means ,the buf have full ,do not need to fill
	if this.w-this.r < n && this.w-this.r < len(this.buf) {
		this.fill()
	}

	if n >= len(this.buf) { //if n is larger than the whole buf ,return the whole buf
		return this.buf[this.r:this.w], nil
	}

	//after fill
	//if our buffer data is still less than n
	//we return the data we can return
	if avail := this.w - this.r; avail < n {
		n = avail
	}
	return this.buf[this.r : this.r+n]
}

func (this *TzzReader) Bufferd() int {
	return this.w - this.r
}

func (this *TzzReader) Discard(n int) (int, error) {
	remain := n
	for {
		skip := this.Bufferd()
		if skip == 0 {
			this.fill()
			skip = this.Bufferd()
		}
		if skip > remain {
			skip = remain
		}
		this.r += skip
		remain -= skip
		if remain == 0 {
			return n, nil
		}
	}
}
