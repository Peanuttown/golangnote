package main

import "github.com/smallnest/rpcx/log"
import "goSdk/tzzbufio"
import "strings"

func main() {
	rd := strings.NewReader("tangzoneze")
	bufrd := tzzbufio.NewTzzReader(5, rd)
	data := make([]byte, 2)
	n, err := bufrd.Read(data)
	if err != nil {
		log.Error(err)
	}
	log.Infof("read data length%d,%+v", n, string(data))

	data = make([]byte, 4)
	n, err = bufrd.Read(data)
	if err != nil {
		log.Error(err)
	}
	log.Infof("read data length%d,%+v", n, string(data))

	temp := data[:2]
	log.Infof("temp:%+v", temp)
	temp[0] = 's'
	log.Infof("after change origin buf:%+v,temp:%+v", string(data), string(temp))

}
