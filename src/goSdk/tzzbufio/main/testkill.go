package main

import (
	"fmt"
)
import "os"
import "os/exec"
import "os/signal"
import "time"

func main() {
	tt := os.Args[1]
	fmt.Println("vim-go")
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c)
		<-c
		cmd := exec.Command("./test","00")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Start()
		time.Sleep(time.Second * 3)
		os.Exit(0)
	}()
	for {
		time.Sleep(time.Second * 2)
		fmt.Println(tt)
	}
}
