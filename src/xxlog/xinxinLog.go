package xxlog

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"log"
	"os"
	"path"
	"time"
)

var gLog *XXLog

var (
	LogLevelDebug = 1
	LogLevelError = 2
)

var (
	LogLevelDebugStr = "debug"
	LogLevelErrorStr = "error"
)

const (
	TIME_FORMAT = "2006-01-02 15:04:05"
)

//func init(){
//	err := InitXXLog(LogLevelDebug,conf.BMWConfig.LeafConfig.LogPath,log.LstdFlags | log.Llongfile)
//	if err != nil {
//		panic(err)
//	}
//}

type XXLog struct {
	level    int
	baseLog  *log.Logger
	baseFile *os.File
}
type logJson struct {
	Level string
	Date  string
	Log   string
}

func newLogJson(level, s string) *logJson {
	l := new(logJson)
	l.Level = level
	l.Date = time.Now().Format(TIME_FORMAT)
	l.Log = s

	return l
}

func (l *logJson) marshal() string {
	data, err := json.Marshal(l)
	if err != nil {
		return errors.New(fmt.Sprintf("marshal jsonLog failed:%s", err.Error())).Error()
	} else {
		return string(data)
	}
}

func InitXXLog(level int, logFilePath string, logFlag int) error {
	l := new(XXLog)
	l.level = level
	if logFilePath == "" {
		l.baseLog = log.New(os.Stdout, "", logFlag)
		l.baseFile = os.Stdout
	} else {
		nowStr := time.Now().Format(TIME_FORMAT)
		fileName := fmt.Sprintf("server_starts_at_%s", nowStr)
		file, err := os.Create(path.Join(logFilePath, fileName))
		if err != nil {
			return err
		}
		l.baseLog = log.New(file, "", logFlag)
		l.baseFile = file
	}
	gLog = l
	return nil
}

func Close() {
	if gLog != nil {
		if gLog.baseFile != nil {
			gLog.baseFile.Close()
		}
		gLog.baseFile = nil
		gLog.baseLog = nil
	}
}

func Debug(s ...interface{}) {
	//if gLog.level > LogLevelDebug {
	//	return
	//}
	l := newLogJson(LogLevelDebugStr, fmt.Sprint(s...))
	gLog.baseLog.Output(2,fmt.Sprintf("[%s] %s",color.BlueString(LogLevelDebugStr),l.marshal()) )
}

func Error(s ...interface{}) {
	l := newLogJson(LogLevelErrorStr, fmt.Sprint(s...))
	gLog.baseLog.Output(2,fmt.Sprintf("[%s] %s",color.RedString(LogLevelErrorStr),l.marshal()))
}
func Errorf(format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	l := newLogJson(LogLevelErrorStr, s)
	gLog.baseLog.Output(2,fmt.Sprintf("[%s] %s",color.RedString(LogLevelErrorStr),l.marshal()) )
}

func Debugf(format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	l := newLogJson(LogLevelDebugStr, s)
	gLog.baseLog.Output(2,fmt.Sprintf("[%s] %s",color.BlueString(LogLevelDebugStr),l.marshal()))
}
