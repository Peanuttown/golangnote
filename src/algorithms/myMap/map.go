package myMap

type TreeNode struct {
	Key        int
	PayLoad    interface{}
	LeftNode   *TreeNode
	RightNode  *TreeNode
	ParentNode *TreeNode
}

func NewTreeNode(key int, val interface{}) *TreeNode {
	t := new(TreeNode)
	t.Key = key
	t.PayLoad = val
	return t
}

func (t *TreeNode) HasLeftChild() bool {
	return t.LeftNode != nil
}

func (t *TreeNode) HasRightChild() bool {
	return t.RightNode != nil
}

func (t *TreeNode) IsLeftChild() bool {
	return t.ParentNode != nil && t.ParentNode.LeftNode == t
}
func (t *TreeNode) IsRightChild() bool {
	return t.ParentNode != nil && t.ParentNode.RightNode == t
}
func (t *TreeNode) IsRoot() bool {
	return t.ParentNode == nil
}
func (t *TreeNode) IsLeaf() bool {
	return !(t.HasLeftChild() || t.HasRightChild())
}

func (t *TreeNode) HasAnyChild() bool {
	return t.HasLeftChild() || t.HasRightChild()
}

func (t *TreeNode) HasBothChilds() bool {
	return t.HasLeftChild() && t.HasRightChild()
}
func (t *TreeNode) ReplaceNodeData(key int, val interface{}, lc, rc *TreeNode) {
	t.Key = key
	t.PayLoad = val
	t.LeftNode = lc
	t.RightNode = rc
	if t.HasLeftChild() {
		t.LeftNode.ParentNode = t
	}
	if t.HasRightChild() {
		t.RightNode.ParentNode = t
	}
}

func (t *TreeNode) Put(key int, val interface{}) {
	if t.ParentNode == nil {
		t.ParentNode = t
	}
	newT := new(TreeNode)
	newT.Key = key
	newT.PayLoad = val
	t.putHelper(newT)
}
func (t *TreeNode) putHelper(newT *TreeNode) {
	if newT.Key < t.Key {
		if t.HasLeftChild() {
			t.LeftNode.putHelper(newT)
		} else {
			t.LeftNode = newT
			newT.ParentNode = t
		}
	} else {
		if t.HasRightChild() {
			t.RightNode.putHelper(newT)
		} else {
			t.RightNode = newT
			newT.ParentNode = t
		}
	}
}

func (t *TreeNode) Get(key int) interface{} {
	if key == t.Key {
		return t.PayLoad
	}
	if key < t.Key {
		if t.HasLeftChild() {
			return t.LeftNode.Get(key)
		}
		return nil
	}
	if key > t.Key {
		if t.HasRightChild() {
			return t.RightNode.Get(key)
		}
		return nil
	}
	return nil
}

func (t *TreeNode) Delete(key int) {
	if key == t.Key {
		if t.HasLeftChild() {
			t.Move()
		} else if t.HasRightChild() {
			t = t.RightNode
		} else {
			if t.IsLeftChild() {
				t.ParentNode.LeftNode = nil
			}
			if t.IsRightChild() {
				t.ParentNode.RightNode = nil

			}
		}
	} else if key < t.Key {
		if t.HasLeftChild() {
			t.LeftNode.Delete(key)
		}
	} else {
		if t.HasRightChild() {
			t.RightNode.Delete(key)
		}
	}
}

func (t *TreeNode) Move() {
	temp := t.RightNode
	t = t.LeftNode
	if t.HasRightChild() {
		t.putHelper(temp)
	}
}
