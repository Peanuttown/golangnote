package myMap

import (
	"fmt"
	"testing"
)

func TestMap(t *testing.T) {
	m := NewTreeNode(1, 11)
	m.Put(3, 33)
	m.Put(2, 22)
	m.Delete(2)
	fmt.Println(m.Get(2))

}
