package mygraph

type AdjMap map[string]int

type Vertex struct {
	Id  string
	Adj AdjMap
}

func NewVertex(id string) *Vertex {
	v := new(Vertex)
	v.Id = id
	v.Adj = make(AdjMap)
	return v
}

func (this *Vertex) AddNeighbor(nbrId string, weight int) {
	this.Adj[nbrId] = weight
}

func (this *Vertex) GetNbrs() []string {
	s := make([]string, len(this.Adj))
	var count int
	for i, _ := range this.Adj {
		s[count] = i
		count++
	}
	return s
}

func (this *Vertex) GetId() string {
	return this.Id
}
func (this *Vertex) GetWeight(nbrId string) int {
	return this.Adj[nbrId]
}

type TzzGraph struct {
	NumVertices int
	VertList    map[string]*Vertex
}

func NewTzzGraph() *TzzGraph {
	g := new(TzzGraph)
	g.VertList = make(map[string]*Vertex)
	return g
}

func (this *TzzGraph) AddVertexById(id string) {
	nv := NewVertex(id)
	this.VertList[id] = nv
	this.NumVertices += 1
}

func (this *TzzGraph) GetVertexById(id string) *Vertex {
	if v, ok := this.VertList[id]; ok {
		return v
	} else {
		return nil
	}
}

func (this *TzzGraph) AddEdge(from, to string, weight int) {
	if _, ok := this.VertList[from]; !ok {
		this.AddVertexById(from)
	}
	if _, ok := this.VertList[to]; !ok {
		this.AddVertexById(to)
	}
	this.VertList[from].AddNeighbor(to, weight)
}

func (this *TzzGraph) GetVerticesIds() []string {
	ids := make([]string, len(this.VertList))
	var count int
	for id, _ := range this.VertList {
		ids[count] = id
		count++
	}
	return ids
}
