package main

import "algorithms/mygraph"
import "fmt"
import "strconv"

func main() {
	graph := mygraph.NewTzzGraph()
	for i := 0; i < 6; i++ {
		id := strconv.Itoa(i)
		graph.AddVertexById(id)
	}
	graph.AddEdge("0", "1", 5)
	graph.AddEdge("0", "5", 2)
	graph.AddEdge("1", "2", 4)
	graph.AddEdge("2", "3", 9)
	graph.AddEdge("3", "4", 7)
	graph.AddEdge("3", "5", 3)
	graph.AddEdge("4", "0", 1)
	graph.AddEdge("5", "4", 8)
	graph.AddEdge("5", "2", 1)
	for _, v := range graph.VertList {
		for nbr, _ := range v.Adj {
			fmt.Printf("(%s:%s)\n", v.Id, nbr)
		}
	}
}
