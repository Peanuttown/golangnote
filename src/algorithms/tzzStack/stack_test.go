package tzzStack

import (
	"github.com/smallnest/rpcx/log"
	"testing"
)

func TestTzzStack(t *testing.T) {
	stack := NewTzzStack()
	log.Infof("this stack is empty?:%v", stack.IsEmpty())
	stack.Push(1)
	stack.Push(2)
	log.Info(stack.Pop())
	log.Info(stack.Pop())
}
