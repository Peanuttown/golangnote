package tzzStack

import (
	"errors"
)

type TzzStack struct {
	Items []interface{}
}

var ErrStackEmpty = errors.New("stack is empty")

func NewTzzStack() *TzzStack {
	s := new(TzzStack)
	s.Items = make([]interface{}, 0, 10)
	return s
}

func (this *TzzStack) Push(item interface{}) {
	this.Items = append(this.Items, item)
}

func (this *TzzStack) Pop() (item interface{}, err error) {
	if this.Len() == 0 {
		err = ErrStackEmpty
		return
	}
	item = this.Items[len(this.Items)-1]
	this.Items = this.Items[:len(this.Items)-1]
	return
}

func (this *TzzStack) Len() int {
	return len(this.Items)
}

func (this *TzzStack) Peek() (item interface{}, err error) {
	if this.Len() == 0 {
		err = ErrStackEmpty
		return
	}
	return this.Items[this.Len()-1], nil

}

func (this *TzzStack) IsEmpty() bool {
	return this.Len() == 0
}
