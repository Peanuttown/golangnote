package tzzTree

import (
	"fmt"
	"testing"
)

func TestNewSearchTree2(t *testing.T) {
	st := NewSearchTree2()
	st.Put(1,"tzz")
	st.Put(4,"s")
	fmt.Println(st.Get(1))
	st.Delete(1)
	fmt.Println(st.Get(1))
}
