package tzzTree

import (
	"container/heap"
	"fmt"
	"github.com/smallnest/rpcx/log"
	"math/rand"
	"testing"
	"time"
)

type PItem struct {
	Priority int
	PayLoad  interface{}
}

type PItems []*PItem

func (this *PItems) Len() int {
	return len(*this)
}

func (this *PItems) Swap(i, j int) {
	(*this)[i], (*this)[j] = (*this)[j], (*this)[i]
}

func (this *PItems) Less(i, j int) bool {
	return (*this)[i].Priority < (*this)[j].Priority
}

func (this *PItems) Push(x interface{}) {
	(*this) = append((*this), x.(*PItem))
}
func (this *PItems) Pop() interface{} {
	retVal := (*this)[len(*this)-1]
	(*this) = (*this)[:len(*this)-1]
	return retVal
}

func (this *PItem) GetPriority() int {
	return this.Priority
}

func TestHeap(t *testing.T) {
	h := NewTzzBinaryHeap()
	items := make([]PriorityItem, 5)
	var count int
	for {
		if count == 5 {
			break
		}
		item := new(PItem)
		rand.Seed(time.Now().Add(time.Second * time.Duration(count)).Unix())
		item.Priority = rand.Intn(10)
		items[count] = item
		count++
	}
	h.BuildHeap(items)
	for i, v := range h.List {
		if i != 0 {
			fmt.Println(v.GetPriority())
		}
	}
	fmt.Println("---------------")
	for {
		if count == 0 {
			break
		}
		m, _ := h.DelMin()
		fmt.Println(m.(*PItem).Priority)
		count--
	}
}

func TestGoHeap(t *testing.T) {
	var bigCount int
	for {
		if bigCount == 10 {
			break
		}
		pitems := make(PItems, 5)
		var count int
		for {
			if count == 5 {
				break
			}
			item := new(PItem)
			rand.Seed(time.Now().Add(time.Second * time.Duration(count*bigCount)).Unix())
			item.Priority = rand.Intn(10)
			pitems[count] = item
			count++
		}
		heap.Init(&pitems)
		for i, _ := range pitems {
			item := heap.Pop(&pitems)
			fmt.Printf("%d:%d\n", i, item.(*PItem).Priority)
		}
		fmt.Println("---------------")
		bigCount++
	}
}

func TestHeap2(t *testing.T) {
	heap := NewBinaryHeap2()
	list := make([]int, 10)
	var count int
	for count < 10 {
		rand.Seed(time.Now().Add(time.Second * time.Duration(count)).Unix())
		list[count] = rand.Intn(50)
		count++
	}
	heap.BuildHeap(list)
	for count > 0 {
		log.Info(heap.DelMin())
		count--
	}
}
