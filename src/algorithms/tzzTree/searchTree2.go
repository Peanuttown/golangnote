package tzzTree

type SearchTree2 struct {
	Root *SearchTreeNode2
	Size int
}

type SearchTreeNode2 struct {
	Key     int
	Payload interface{}
	Parent  *SearchTreeNode2
	LC      *SearchTreeNode2
	RC      *SearchTreeNode2
}

func NewSearchTreeNode2(key int, payload interface{}) *SearchTreeNode2 {
	node := new(SearchTreeNode2)
	node.Key = key
	node.Payload = payload
	return node

}

func NewSearchTree2() *SearchTree2 {
	tr := new(SearchTree2)
	return tr
}

func (this *SearchTreeNode2) IsLeaf() bool {
	return this.LC == nil && this.RC == nil
}

func (this *SearchTreeNode2) HasAnyChild() bool {
	return (this.LC != nil) || (this.RC != nil)
}

func (this *SearchTreeNode2) HasBothChild() bool {
	return this.LC != nil && this.RC != nil
}

func (this *SearchTree2) Put(key int, value interface{}) {
	if this.Root == nil {
		this.Root = NewSearchTreeNode2(key, value)
	} else {
		currentNode := this.Root
		for currentNode != nil {
			if currentNode.Key == key {
				currentNode.Payload = value
				return
			}
			if key < currentNode.Key {
				currentNode = currentNode.LC
			} else {
				currentNode = currentNode.RC
			}
		}
	}
}

func (this *SearchTree2) Get(key int) *SearchTreeNode2 {
	currentNode := this.Root
	for currentNode != nil {
		if currentNode.Key == key {
			return currentNode
		}
		if key < currentNode.Key {
			currentNode = currentNode.LC
		} else {
			currentNode = currentNode.RC
		}
	}
	return currentNode
}

func (this *SearchTreeNode2) HasParent() bool {
	return this.Parent != nil
}

func (this *SearchTreeNode2) HasLC() bool {
	return this.LC != nil
}

func (this *SearchTreeNode2) HasRC() bool {
	return this.RC != nil
}

func (this *SearchTreeNode2) IsLC() bool{
	return this.Parent.LC == this
}

func (this *SearchTreeNode2) IsRC() bool{
	return this.Parent.RC == this
}

func (this *SearchTree2) Delete(key int) {
	node := this.Get(key)
	if node == nil {
		return
	}
	if node.IsLeaf() {
		if node.HasParent() {
			if node.IsLC() {
				node.Parent.LC = nil
			} else {
				node.Parent.RC = nil
			}
		} else { //root
			this.Root = nil
		}
	} else if node.HasAnyChild() {
		if node.HasParent() {
			if node.IsLC() {
				if node.HasLC() {
					node.LC.Parent = node.Parent
					node.Parent.LC = node.LC
				} else {
					node.RC.Parent = node.Parent
					node.Parent.LC = node.RC
				}
			} else {
				if node.HasLC() {
					node.LC.Parent = node.Parent
					node.Parent.RC = node.LC
				} else {
					node.RC.Parent = node.Parent
					node.Parent.RC = node.RC
				}
			}
		} else { //root
			if node.HasLC() {
				this.Root = node.LC
				node.LC.Parent = nil
			} else {
				this.Root = node.RC
				node.RC.Parent = nil
			}
		}
	} else {
		mc := this.FindMin(node.RC)
		node.Key = mc.Key
		node.Payload = mc.Payload
		this.SplicOut(mc)
	}
}

func (this *SearchTree2) FindMin(node *SearchTreeNode2) *SearchTreeNode2 {
	currentNode := node
	for currentNode.LC != nil {
		currentNode = currentNode.LC
	}
	return currentNode
}

func (this *SearchTree2) SplicOut(node *SearchTreeNode2) {
	if node.HasRC() {
		if node.IsLC() {
			node.Parent.LC = node.RC
			node.RC.Parent = node.Parent
		} else {
			node.Parent.RC = node.RC
			node.RC.Parent = node.Parent
		}
	} else {
		if node.IsLC() {
			node.Parent.LC = nil
		} else {
			node.Parent.RC = nil
		}
	}
}
