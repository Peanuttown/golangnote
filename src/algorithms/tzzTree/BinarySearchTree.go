package tzzTree

type BinarySearchTreeNode struct {
	Key     int
	Payload interface{}
	Parent  *BinarySearchTreeNode
	LC      *BinarySearchTreeNode
	RC      *BinarySearchTreeNode
}

func NewBinarySearchTreeNode(key int, value interface{}) *BinarySearchTreeNode {
	n := new(BinarySearchTreeNode)
	n.Key = key
	n.Payload = value
	return n
}
func (this *BinarySearchTreeNode) IsLeaf() bool {
	return this.LC == nil && this.RC == nil
}

func (this *BinarySearchTreeNode) HasBothChild() bool {
	return this.LC != nil && this.RC != nil
}

func (this *BinarySearchTreeNode) HasLeftChild() bool {
	return this.LC != nil
}
func (this *BinarySearchTreeNode) HasRightChild() bool {
	return this.RC != nil
}
func (this *BinarySearchTreeNode) IsLC() bool {
	if this.Parent == nil {
		return false
	}
	return this.Parent.LC == this
}
func (this *BinarySearchTreeNode) IsRC() bool {
	if this.Parent == nil {
		return false
	}
	return this.Parent.RC == this
}

func (this *BinarySearchTreeNode) HasOneChild() bool {
	return (this.LC == nil && this.RC != nil) || (this.LC != nil && this.RC == nil)

}

func (this *BinarySearchTreeNode) RemoveSelf() {
	if this.Parent != nil {
		if this.IsLC() {
			this.Parent.LC = nil
		} else {
			this.Parent.RC = nil
		}
	}
}

func (this *BinarySearchTreeNode) HasLC() bool {
	return this.LC != nil
}
func (this *BinarySearchTreeNode) HasRC() bool {
	return this.RC != nil
}

type SearchTree struct {
	Root *BinarySearchTreeNode
	Size int
}

func NewSearchTree() *SearchTree {
	t := new(SearchTree)
	return t
}

func (this *SearchTree) Put(key int, value interface{}) {
	if this.Root == nil {
		this.Root = NewBinarySearchTreeNode(key, value)
	} else {
		this.putHelper(key, value, this.Root)
	}
}

func (this *SearchTree) putHelper(key int, value interface{}, node *BinarySearchTreeNode) {
	if key < node.Key {
		if node.HasLC() {
			this.putHelper(key, value, node.LC)
		} else {
			node.LC = NewBinarySearchTreeNode(key, value)
			node.LC.Parent = node
		}
	} else {
		if node.HasRC() {
			this.putHelper(key, value, node.RC)
		} else {
			node.RC = NewBinarySearchTreeNode(key, value)
			node.RC.Parent = node
		}
	}
}

func (this *SearchTree) Get(key int) (node *BinarySearchTreeNode, ok bool) {
	currentNode := this.Root
	for !ok {
		if currentNode == nil {
			return
		}
		if currentNode.Key == key {
			node = currentNode
			ok = true
			return
		}
		if key < currentNode.Key {
			currentNode = currentNode.LC
		} else {
			currentNode = currentNode.RC
		}
	}
	return
}

func (this *SearchTree) Delete(key int) {
	rmNode, ok := this.Get(key)
	if ok {
		this.remove(rmNode)
	}
}

func (this *SearchTree) remove(node *BinarySearchTreeNode) {
	if node.IsLeaf() {
		node.RemoveSelf()
	} else if node.HasOneChild() {
		if node.IsLC() {
			node.RemoveSelf()
			if node.HasLC() {
				node.LC.Parent = node.Parent
				node.Parent.LC = node.LC
			} else {
				node.RC.Parent = node.Parent
				node.Parent.LC = node.RC
			}
		} else {
			node.RemoveSelf()
			if node.HasLC() {
				node.LC.Parent = node.Parent
				node.Parent.RC = node.LC
			} else {
				node.RC.Parent = node.Parent
				node.Parent.RC = node.RC
			}
		}
	}else{//has two child
		sucNode := this.FindMin(node.RC)
		this.SpliceOut(sucNode)
		node.Key = sucNode.Key
		node.Payload = sucNode.Payload
	}
}

func (this *SearchTree) FindMin(node *BinarySearchTreeNode) *BinarySearchTreeNode{
	currentNode := node
	for{
		if node.HasLC(){

			currentNode = node.LC
		}else{
			break
		}
	}
	return currentNode
}

func (this *SearchTree) SpliceOut(node *BinarySearchTreeNode){
	if node.IsLeaf(){
		if node.IsLC(){
			node.Parent.LC = nil
		}else{
			node.Parent.RC = nil
		}
	}else{
		if node.IsRC(){
			panic("wrong")
		}
		node.RC.Parent = node.Parent
		node.Parent.LC = node.RC
	}
}

