package tzzTree

type BinaryHeap2 struct {
	List []int
	Size int
}

func NewBinaryHeap2() *BinaryHeap2 {
	h := new(BinaryHeap2)
	h.List = make([]int, 1)
	h.List[0] = 0
	return h
}

func (this *BinaryHeap2) Len() int {
	return this.Size
}

func (this *BinaryHeap2) IsEmpty() bool {
	return this.Len() == 0
}

func (this *BinaryHeap2) Insert(i int) {
	this.List = append(this.List, i)
	this.Size += 1
	this.percUp(this.Size)
}

func (this *BinaryHeap2) percUp(index int) {
	for index/2 > 0 {
		if this.List[index] < this.List[index/2] {
			this.Swap(index, index/2)
		} else {
			break
		}
	}
}

func (this *BinaryHeap2) DelMin() int {
	retVal := this.List[1]
	this.List[1] = this.List[len(this.List)-1]
	this.Pop()
	this.Size--
	this.percDown(1)
	return retVal
}

func (this *BinaryHeap2) percDown(index int) {
	for index*2 < this.Len() { //has child
		mc := this.GetMinChild(index)
		if this.List[index] > this.List[mc] {
			this.Swap(index, mc)
			index *= 2
		} else {
			break
		}
	}
}

func (this *BinaryHeap2) GetMinChild(index int) int {
	lc := index * 2
	rc := lc + 1
	if rc > this.Len() {
		return lc
	}
	if this.List[lc] < this.List[rc] {
		return lc
	} else {
		return rc
	}
}

func (this *BinaryHeap2) Pop() {
	this.List = this.List[:len(this.List)-1]
}

func (this *BinaryHeap2) Swap(i, j int) {
	this.List[i], this.List[j] = this.List[j], this.List[i]
}

func (this *BinaryHeap2) BuildHeap(list []int) {
	this.List = append(this.List, list...)
	this.Size += len(list)
	index := len(list) / 2
	for index > 0 {
		this.percDown(index)
		index--

	}
}
