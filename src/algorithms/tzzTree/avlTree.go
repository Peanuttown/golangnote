package tzzTree

type AVLTree struct {
	Root *SearchTree2
	Size int
}

func (this *AVLTree) Put(key int, value interface{}) {
	if this.Root == nil {
		this.Root = NewSearchTreeNode2(key, value)
		return
	}

}

func (this *AVLTree) putHelper(key int, value interface{}, node *SearchTreeNode2) {
	if key < node.Key {
		if node.HasLC() {
			this.putHelper()

		}
	}
}
