package tzzTree

import (
	"fmt"
	"strings"
	"testing"
)

func TestStringsSplit(t *testing.T) {
	ss := "tzz"
	l := strings.Split(ss, "")
	t.Log(l)
}

func TestParseTree(t *testing.T) {
	exp := "((1+2)+(2/2))"
	tt := BuildParseTree2(exp)
	r := Evaluate(tt)
	fmt.Println(r)

}
