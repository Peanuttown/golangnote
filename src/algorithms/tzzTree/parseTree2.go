package tzzTree

import (
	"algorithms/tzzStack"
	"strconv"
	"strings"
)

func BuildParseTree2(fullParentheseExp string) *BinaryTree {
	expList := strings.Split(fullParentheseExp,"")
	expTree := NewBinaryTree("")
	eStack := tzzStack.NewTzzStack()
	eStack.Push(expTree)
	current := expTree
	for _, v := range expList {
		if IsLeftParenthese(v) {
			current.InsertLeft("")
			eStack.Push(current)
			current = current.LeftC
		} else if IsOperator(v) {
			current.SetKey(v)
			current.InsertRight(v)
			eStack.Push(current)
			current = current.RightC
		} else if IsRightParenthese(v) {
			temp,_ := eStack.Pop()
			current = temp.(*BinaryTree)
		} else {
			current.SetKey(v)
			temp,_:= eStack.Pop()
			current = temp.(*BinaryTree)
		}
	}
	return expTree
}

func Evaluate(t *BinaryTree) int {
	op := t.GetKey().(string)
	if fn, ok := OpMap[op]; ok {
		return fn(Evaluate(t.LeftC), Evaluate(t.RightC))
	} else {
		 r,_:=strconv.Atoi(t.GetKey().(string))
		 return r
	}
}

var OpMap = map[string]func(int, int) int{
	"+":Add,
	"-":Sub,
	"*":Multi,
	"/":Div,
}

func Add(i, j int) int {
	return i + j
}

func Sub(i, j int) int {
	return i - j
}

func Multi(i, j int) int {
	return i * j
}

func Div(i, j int) int {
	return i / j
}

func IsOperator(s string) bool {
	var operators = []string{"+", "*", "/", "-"}
	for _, v := range operators {
		if v == s {
			return true
		}
	}
	return false
}

func IsLeftParenthese(s string) bool {
	return s == "("
}

func IsRightParenthese(s string) bool {
	return s == ")"

}
