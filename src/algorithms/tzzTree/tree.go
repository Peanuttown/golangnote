package tzzTree

import (
	"fmt"
)

type TzzBinaryTree struct {
	Key        interface{}
	LeftChild  *TzzBinaryTree
	RightChild *TzzBinaryTree
}

func NewTzzBinaryTree(key interface{}) *TzzBinaryTree {
	t := new(TzzBinaryTree)
	t.Key = key
	return t
}

func (this *TzzBinaryTree) InsertLeft(key interface{}) {
	if this.LeftChild == nil {
		this.LeftChild = NewTzzBinaryTree(key)
	} else {
		t := NewTzzBinaryTree(key)
		t.LeftChild = this.LeftChild
		this.LeftChild = t
	}

}
func (this *TzzBinaryTree) InsertRight(key interface{}) {
	if this.RightChild == nil {
		this.RightChild = NewTzzBinaryTree(key)
	} else {
		t := NewTzzBinaryTree(key)
		t.RightChild = this.RightChild
		this.RightChild = t
	}
}

func PreOrder(t *TzzBinaryTree) {
	if t != nil {
		fmt.Println(t.GetKey())
		PreOrder(t.LeftChild)
		PreOrder(t.RightChild)
	}
}

func (this *TzzBinaryTree) GetKey() interface{}{
	return this.Key
}

func (this *TzzBinaryTree) SetKey(key interface{}){
	this.Key = key
}
