package tzzTree

import (
	"fmt"
	"testing"
)

func TestNewBinarySearchTreeNode(t *testing.T) {
	tree := NewSearchTree()
	tree.Put(1,"tzz")
	tree.Put(3,"tang")
	fmt.Println(tree.Get(1))
	fmt.Println(tree.Get(3))
	tree.Delete(3)
	fmt.Println(tree.Get(3))

}

