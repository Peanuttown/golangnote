package tzzTree

import (
	"errors"
)

var ErrEmptyHeap = errors.New("empty heap")

const (
	StarIndex = 1
)

type PriorityItem interface {
	GetPriority() int
}

type TzzBinaryHeap struct {
	List []PriorityItem
	Size int
}

func NewTzzBinaryHeap() *TzzBinaryHeap {
	h := new(TzzBinaryHeap)
	h.List = make([]PriorityItem, 1)
	h.List[0] = nil
	return h
}

func (this *TzzBinaryHeap) Insert(item PriorityItem) {
	this.List = append(this.List, item)
	this.Size += 1
	this.perup(this.Size)
}



func (this *TzzBinaryHeap) GetSize() int {
	return this.Size

}

func (this *TzzBinaryHeap) IsEmpey() bool {
	return this.Size == 0

}

func (this *TzzBinaryHeap) perup(i int) {
	for i/2 > 0 {
		if this.List[i].GetPriority() < this.List[i/2].GetPriority() {
			this.List[i], this.List[i/2] = this.List[i/2], this.List[i]
		}
		i /= 2
	}
}

func (this *TzzBinaryHeap) Pop() {
	this.List = this.List[:len(this.List)-1]
}

func (this *TzzBinaryHeap) DelMin() (interface{}, error) {
	if !this.IsEmpey() {
		r := this.List[1]
		this.List[1] = this.List[this.Size]
		this.Size -= 1
		this.Pop()
		this.perdown(1)
		return r, nil
	} else {
		return nil, errors.New("heap is empty")
	}
}

func (this *TzzBinaryHeap) GetMin() (interface{}, error) {
	if this.IsEmpey() {
		return nil, ErrEmptyHeap
	} else {
		return this.List[StarIndex], nil
	}
}

func (this *TzzBinaryHeap) perdown(i int) {
	for i*2 <= this.GetSize() {
		mc := this.minChild(i)
		if this.List[i].GetPriority() > this.List[mc].GetPriority() {
			this.Swap(i, mc)
		}
		i = i * 2
	}
}

func (this *TzzBinaryHeap) Swap(i, j int) {
	this.List[i], this.List[j] = this.List[j], this.List[i]

}

func (this *TzzBinaryHeap) minChild(i int) int {
	chIndex := i * 2
	if this.Size < chIndex+1 {
		return chIndex
	}
	if this.List[chIndex].GetPriority() < this.List[chIndex+1].GetPriority() {
		return chIndex
	} else {
		return chIndex + 1
	}
}

func (this *TzzBinaryHeap) BuildHeap(items []PriorityItem) error {
	if !this.IsEmpey() {
		return errors.New("this is not a empty items")
	}
	this.List = append(this.List, items...)
	this.Size = len(items)
	i := this.GetSize() / 2
	for i > 0 {
		this.perdown(i)
		i = i - 1
	}
	return nil
}
