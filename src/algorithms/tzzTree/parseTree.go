package tzzTree

import (
	"algorithms/tzzStack"
	"strings"
)

func BuildParseTree(fpexp string) *TzzBinaryTree {
	expList := strings.Split(fpexp, "")
	expTree := NewTzzBinaryTree("")
	stack := tzzStack.NewTzzStack()
	current := expTree
	for _, v := range expList {
		if v == "(" {
			current.InsertLeft("")
			stack.Push(current)
			current = current.LeftChild
		} else if v == "+" || v == "-" {
			current.SetKey(v)
			current.InsertRight("")
			stack.Push(current)
			current = current.RightChild
		} else if v == ")" {
			currentT,_ := stack.Pop()
			current = currentT.(*TzzBinaryTree)
		} else {
			current.SetKey(v)
			currentT,_ := stack.Pop()
			current = currentT.(*TzzBinaryTree)
		}
	}
	return expTree
}

