package tzzTree

type BinaryTree struct {
	Key    interface{}
	LeftC  *BinaryTree
	RightC *BinaryTree
}

func NewBinaryTree(key interface{}) *BinaryTree {
	t := new(BinaryTree)
	t.Key = key
	return t
}

func (this *BinaryTree) InsertLeft(key interface{}) {
	if this.LeftC == nil {
		this.LeftC = NewBinaryTree(key)
	} else {
		t := NewBinaryTree(key)
		t.LeftC = this.LeftC
		this.LeftC = t
	}
}

func (this *BinaryTree) InsertRight(key interface{}) {
	if this.RightC == nil {
		this.RightC = NewBinaryTree(key)
	} else {
		t := NewBinaryTree(key)
		t.RightC = this.RightC
		this.RightC = t
	}
}

func (this *BinaryTree) GetKey() interface{} {
	return this.Key
}
func (this *BinaryTree) SetKey(key interface{}) {
	this.Key = key

}
