package tzzTree

import (
	"github.com/smallnest/rpcx/log"
	"testing"
)

func TestTzzTree(t *testing.T) {
	te := NewTzzBinaryTree("tzz")
	te.InsertLeft("mother")
	te.InsertRight("father")
	log.Info(te.LeftChild.Key)
	log.Info(te.RightChild.Key)

}
