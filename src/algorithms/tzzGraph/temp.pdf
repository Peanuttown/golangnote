The term computer graphics describes any use of computers to create and manipulate
images. This book introduces the algorithmic and mathematical tools
that can be used to create all kinds of images—realistic visual effects, informative
technical illustrations, or beautiful computer animations. Graphics can be two- or
three-dimensional; images can be completely synthetic or can be produced by manipulating
photographs. This book is about the fundamental algorithms and mathematics,
especially those used to produce synthetic images of three-dimensional
objects and scenes.

Actually doing computer graphics inevitably requires knowing about specific
hardware, file formats, and usually a graphics API (see Section 1.3) or two.
Computer graphics is a rapidly evolving field, so the specifics of that knowledge 
are a moving target. Therefore, in this book we do our best to avoid depending
on any specific hardware or API. Readers are encouraged to supplement the text
with relevant documentation for their software and hardware environment. Fortunately,
the culture of computer graphics has enough standard terminology and
concepts that the discussion in this book should map nicely to most environments.
This chapter defines some basic terminology, and provides some historical
background as well as information sources related to computer graphics.
