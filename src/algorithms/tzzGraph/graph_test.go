package tzzGraph

import (
	"github.com/smallnest/rpcx/log"
	"testing"
)

func TestGraph(t *testing.T) {
	g := NewGraph()
	newV1 := NewVertex("vertex1")
	newV2 := NewVertex("vertex2")
	newV3 := NewVertex("vertex3")
	g.AddVertex(newV1)
	g.AddVertex(newV2)
	g.AddVertex(newV3)
	newV3.AddNbr(newV1, 10)
	log.Infof("%#v\n", g)
	log.Infof("%#v\n", newV3)
}

var wordList = []string{"pope", "rope", "nope", "hope", "lope", "pipe", "pape", "pose", "poke"}

func TestBuildGraph(t *testing.T) {
	BuildGraph(wordList)
}
