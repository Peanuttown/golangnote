package tzzGraph

import (
	"github.com/smallnest/rpcx/log"
)

type VertexList map[interface{}]*Vertex
type Nbrs map[*Vertex]int // vertex => weight

type Graph struct {
	Size     int
	Vertices VertexList
}

func NewGraph() *Graph {
	g := new(Graph)
	g.Vertices = make(map[interface{}]*Vertex)
	return g
}

func (this *Graph) AddVertex(vertex *Vertex) {
	this.Vertices[vertex.Id] = vertex
	this.Size++
}

type Vertex struct {
	Id   interface{}
	Nbrs Nbrs
}

func NewVertex(id interface{}) *Vertex {
	v := new(Vertex)
	v.Id = id
	v.Nbrs = make(Nbrs)
	return v
}

func (this *Vertex) AddNbr(vertex *Vertex, weight int) {
	this.Nbrs[vertex] = weight
}

func BuildGraph(wordList []string) *Graph {
	buckets := make(map[string][]string)
	for _, word := range wordList {
		for i, _ := range word {
			var bucket string
			if i == len(word)-1 {
				bucket = string(word[:i]) + "_"
			} else {
				bucket = string(word[:i]) + "_" + string(word[i+1:])
			}
			if _, has := buckets[bucket]; has {
				buckets[bucket] = append(buckets[bucket], word)
			} else {
				buckets[bucket] = make([]string, 0, 5)
			}
		}
	}
	log.Info(buckets)
	return nil

}
